<?php
  class PostContent extends CI_Controller {

    public function postPhoto()
    {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $userID = $this->input->post('userID');
        $name = $this->input->post('photoname');
        $photo = $this->input->post('photo');
        $decodedImage = base64_decode($photo);
        $uploadPath = "uploads/".$name.'.jpg';

        if(file_put_contents("uploads/".$name.'.jpg', $decodedImage)) {
          if($this->ModelPostContent->postPhoto($userID, $name, $uploadPath)) {
            $data = array(
              'result_code' => 1,
              'message' => 'Success to save photo'
            );
          }
          else {
            $data = array(
              'result_code' => 0,
              'message' => 'Failed to save to database'
            );
          }
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to upload photo'
          );
        }
        $savearray = array();
        array_push($savearray, $data);

        echo json_encode($savearray);
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    public function adding() {
      $this->load->model('ModelPostContent');

      for($a = 0; $a < 100; $a++) {
        $sender = 1;
        $imagepath = "/uploads/image1.jpg";
        $this->ModelPostContent->postPhoto($sender, $imagepath);
      }
    }

    public function getPost() {
      $this->load->model('ModelPostContent');

      $limit1 = $this->input->get('lim1');
      $limit2 = $this->input->get('lim2');

      $arrayholder = array();
      if($result = $this->ModelPostContent->retrievingPost($limit1, $limit2)) {
        $data = array(
          'result' => $result
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
    }
  }

    public function retrievingPost()
    {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        if($result = $this->ModelPostContent->retrievingPost()) {
          $data = array(
            'result' => $result
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'It seems there are no post yet'
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    public function deletePost()
    {

      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $id = $this->input->post('id');

        if($result = $this->ModelPostContent->deletePost($id)) {
          $data = array(
            'result_code' => 1,
            'message' => 'Success to delete photo'
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to delete photo'
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    public function retrievingUserPost()
    {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');
      $username = $this->input->post('username');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        if($result = $this->ModelPostContent->retrievingUserPost($username)) {
          $data = array(
            'result' => $result
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'It seems there are no post yet'
          );
          array_push($arrayholder, $data);
          echo json_encode($arrayholder);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

  }
