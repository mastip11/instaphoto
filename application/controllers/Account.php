<?php
  class Account extends CI_Controller {

//login
    public function checkLogin() {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $this->load->model('ModelHolder');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $dataarray = array();

        if(($result = $this->ModelHolder->checkLogin($username, $password)) != "") {
          $data2 = array(
            'result_code' => 1
          );
          $data['result'] = array(
            'id' => $result->id,
            'username' => $result->username,
            'fullname' => $result->fullname,
            'isAdmin' => $result->isAdmin,
            'isActive' => $result->isActive
          );
          array_push($dataarray, $data2);
          array_push($dataarray, $data);
          echo json_encode($dataarray);
        }
        else {
          $data2 = array(
            'result_code' => 0,
            'message' => 'Username/password is wrong'
          );
          array_push($dataarray, $data2);
          echo json_encode($dataarray);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

//register
    public function registerAccount() {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $this->load->model('ModelHolder');
        $username = $this->input->post('username');
        $fullname = $this->input->post('fullname');
        $password = $this->input->post('password');
        $email = $this->input->post('email');

        if($this->ModelHolder->registerAccount($username, $fullname, $password, $email)) {
          $data = array(
            'result_code' => 1,
            'message' => 'Success to register'
          );
          echo json_encode($data);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to register'
          );
          echo json_encode($data);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    // update
    public function updateAccount($username, $fullname, $email) {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $this->load->model('ModelHolder');
        $username = $this->input->post('username');
        $fullname = $this->input->post('fullname');
        $password = $this->input->post('password');

        if($this->ModelHolder->updateAccount($username, $fullname, $password)) {
          $data = array(
            'result_code' => 1,
            'message' => 'Success to update account'
          );
          echo json_encode($data);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to update'
          );
          echo json_encode($data);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    public function blockAccount()
    {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $this->load->model('ModelHolder');
        $username = $this->input->post('username');

        if($this->ModelHolder->blockAccount($username)) {
          $data = array(
            'result_code' => 1,
            'message' => 'Success to block account'
          );
          echo json_encode($data);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to block account'
          );
          echo json_encode($data);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }

    public function unblockAccount()
    {
      $this->load->model('ModelPostContent');

      $key = $this->input->post('api_key');

      $arrayholder = array();

      if($key == $this->ModelPostContent->retrieveKey()->auth_code) {
        $this->load->model('ModelHolder');
        $username = $this->input->post('username');

        if($this->ModelHolder->unblockAccount($username)) {
          $data = array(
            'result_code' => 1,
            'message' => 'Success to unblock account'
          );
          echo json_encode($data);
        }
        else {
          $data = array(
            'result_code' => 0,
            'message' => 'Failed to unblock account'
          );
          echo json_encode($data);
        }
      }
      else {
        $data = array(
          'result_code' => 99,
          'result' => 'Authentication is needed'
        );
        array_push($arrayholder, $data);
        echo json_encode($arrayholder);
      }
    }
  }
