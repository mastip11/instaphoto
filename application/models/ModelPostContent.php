<?php

class ModelPostContent extends CI_Model {

  public function postPhoto($userID, $uploadPath)
  {
    $query = "insert into tb_post (id_sender, imagepath, time, isAllowed) values ('$userID', '$uploadPath', 1)";
    return $this->db->query($query);
  }

  public function retrieveKey()
  {
    $query = "select * from tb_auth";
    return $this->db->query($query)->row();
  }

  public function retrievingPost($limit1, $limit2)
  {
    $query = "select * from tb_post limit $limit1,$limit2";
    return $this->db->query($query)->result();
  }

  public function deletePost($idPost)
  {
    $query = "update tb_post set isAllowed = 0 where id = '$idPost";
    return $this->db->query($query);
  }

  public function retrievingUserPost($username)
  {
    $query = "select id, username, imagepath, time_to_sec(timediff(now(), time)) as time, isAllowed from view_post where username = '$username'";
    return $this->db->query($query)->result();
  }
}
